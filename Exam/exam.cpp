//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "exam.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void __fastcall TForm1::btexitClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::btn_aboutClick(TObject *Sender)
{

	ShowMessage("��� ���� - ��������. n/ ���������� ����� �������");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::btstartClick(TObject *Sender)
{
	TabControl1->GotoVisibleTab(game->Index);
	change_lvl();
//	ShowMessage(min_posible);
//	ShowMessage(max_posible);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::lvl_selectClick(TObject *Sender)
{
	TabControl1->GotoVisibleTab(lvl->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::lvl_easyClick(TObject *Sender)
{
	lvl_index = 0;
	label_lvl->Text = lvl_easy->Text + " �������";
	TabControl1->GotoVisibleTab(inlet->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::lvl_mediumClick(TObject *Sender)
{
	lvl_index = 1;
	label_lvl->Text = lvl_medium->Text + " �������";
	TabControl1->GotoVisibleTab(inlet->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::lvl_hardClick(TObject *Sender)
{
	lvl_index = 2;
	label_lvl->Text = lvl_hard->Text + " �������";
	TabControl1->GotoVisibleTab(inlet->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	TabControl1->GotoVisibleTab(inlet->Index);
}

void TForm1::DoContinue()
{
	change_lvl();
	for (int i=0; i < count_box; i++)
		((TRectangle*)pListBox->Items[i])->Fill->Color = TAlphaColorRec::White;



	FNumberCorrect = RandomRange(min_posible, max_posible);
	
	//������������ ��������� ������
	int *rand_arr = new int[FNumberCorrect];
	TList__1<int> *xTemp=new TList__1<int>;
			xTemp->Capacity = FNumberCorrect;
		try {
			for(int i = 0;i<FNumberCorrect;i++){
				int x = Random(count_box);
				while (xTemp->IndexOf (x)!=-1){
					x++;
					if (x >= count_box) {
						x=0;
					}
				}
				xTemp->Add(x);
				rand_arr[i]=x;
			}

		}
		__finally 
		{
			delete xTemp;
		}
		
//	for(int i = 0; i < FNumberCorrect; i++)
//	{
//		rand_arr[i] = i;
//	}
//	
//	int min;
//	int max;
//	min=rand_arr[0];
//	max=rand_arr[0];
//		for (int i=0; i<FNumberCorrect; i++)
//		{
//			if (rand_arr[i] < min)
//			{
//				min = rand_arr[i];
//			}
//			if (rand_arr[i] > max)
//			{
//				max = rand_arr[i];
//			}
//		}
//	int t = max - min + 1;
//	for (int i=0; i<FNumberCorrect; i++)
//	{
//		rand_arr[i] = rand() % FNumberCorrect + 1;
//		if (rand_arr[i-1] == rand_arr[i])
//		{
//		   i--;   
//		}
//	}

	
	for (int i=0; i<FNumberCorrect; i++)
		((TRectangle*)pListBox->Items[rand_arr[i]])->Fill->Color = TAlphaColorRec::Green;


}

//int TForm1::aValue_test()
//{
//	int val;
//	for (int i=0; i<answ_text->Text.Length(); i++)
//	{
//		if(!isdigit(answ_text->Text[i]))
//		{
//			ShowMessage("����� �����!");
//			break;
//		}
//		else
//		{
//		   val = 
//		}
//	}                                
//}


int TForm1::aValue()
{
//	std::string val;
	int val;
	bool check = true;
	int count_symb = answ_text->Text.Length();
//	char symb[256];
//	int res;
//	if(!isdigit(StrToInt(answ_text->Text)))

//	val = StrToInt(answ_text->Text);
//	if(!isdigit(val))
//	{
//		ShowMessage("yes");
//		return val;
//	}else
//	{
//		val = 404;
//	}
//	
//	return val;	
//	ShowMessage(answ_text->Text[2]);	   
	for (int i = 1; i<count_symb+1; i++)
	{
		if(!isdigit(answ_text->Text[i]))
		{
			check = false;
			ShowMessage("������� �����!");
			break;
        }
	}
	if(check)
	{
		val = StrToInt(answ_text->Text);
		return val;
    }


}

void TForm1::DoAnswer()
{
	int val = aValue();
	answ_text->Text = "";
//	ShowMessage(IntToStr(FNumberCorrect));

	if (val == FNumberCorrect)
		FCountCorrect++;
	else
		FCountWrong++;

	if (FCountWrong >= 2){
		list_result->Items->Add("�������: " + IntToStr(FCountWrong));
		list_result->Items->Add("�����: " + IntToStr(FCountCorrect));
		TabControl1->GotoVisibleTab(outlet->Index);
	}


	DoContinue();
}

void TForm1::DoReset()
{
	FCountCorrect = 0;
	FCountWrong = 0;
	game->Enabled = true;
	list_result->Clear();
	DoContinue();
}

void TForm1::change_lvl()
{
	if (lvl_index == 0)
		{
			min_posible = 2;
			max_posible = 6;
		}
	if (lvl_index == 1)
		{
			min_posible = 4;
			max_posible = 14;
		}
	if (lvl_index == 2)
		{
			min_posible = 10;
			max_posible = 22;
        }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	pListBox = new TList;
	for (int i=1; i<=count_box; i++){
		pListBox->Add(this->FindComponent("Rectangle"+IntToStr(i)));
	}
	DoContinue();
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------


void __fastcall TForm1::reset_btnClick(TObject *Sender)
{
    DoReset();
}
//---------------------------------------------------------------------------




void __fastcall TForm1::answ_btnClick(TObject *Sender)
{
	DoAnswer();	
}
//---------------------------------------------------------------------------

