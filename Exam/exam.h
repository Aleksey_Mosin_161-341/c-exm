//---------------------------------------------------------------------------

#ifndef examH
#define examH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Graphics.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TTabControl *TabControl1;
	TTabItem *inlet;
	TTabItem *game;
	TTabItem *outlet;
	TLayout *Layout_cont;
	TLayout *layout_rect;
	TLayout *layout_answ;
	TLayout *layout_cont_1;
	TButton *btstart;
	TLayout *layout_button;
	TButton *btexit;
	TButton *btn_about;
	TButton *lvl_select;
	TGridPanelLayout *GridPanelLayout1;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TRectangle *Rectangle3;
	TRectangle *Rectangle4;
	TRectangle *Rectangle5;
	TRectangle *Rectangle6;
	TRectangle *Rectangle7;
	TRectangle *Rectangle8;
	TRectangle *Rectangle9;
	TRectangle *Rectangle10;
	TRectangle *Rectangle11;
	TRectangle *Rectangle12;
	TRectangle *Rectangle13;
	TRectangle *Rectangle14;
	TRectangle *Rectangle15;
	TRectangle *Rectangle16;
	TRectangle *Rectangle17;
	TRectangle *Rectangle18;
	TRectangle *Rectangle19;
	TRectangle *Rectangle20;
	TRectangle *Rectangle21;
	TRectangle *Rectangle22;
	TRectangle *Rectangle23;
	TRectangle *Rectangle24;
	TRectangle *Rectangle25;
	TTabItem *lvl;
	TLayout *layout_cont_2;
	TLayout *layout_lvl;
	TButton *lvl_easy;
	TButton *lvl_medium;
	TButton *lvl_hard;
	TLabel *label_lvl;
	TButton *Button1;
	TButton *reset_btn;
	TLayout *layout_cont_3;
	TListBox *list_result;
	TLabel *Label1;
	TBrushObject *Brush_white;
	TBrushObject *Brush_green;
	TEdit *answ_text;
	TButton *answ_btn;
	void __fastcall btexitClick(TObject *Sender);
	void __fastcall btn_aboutClick(TObject *Sender);
	void __fastcall btstartClick(TObject *Sender);
	void __fastcall lvl_selectClick(TObject *Sender);
	void __fastcall lvl_easyClick(TObject *Sender);
	void __fastcall lvl_mediumClick(TObject *Sender);
	void __fastcall lvl_hardClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall reset_btnClick(TObject *Sender);
	void __fastcall answ_btnClick(TObject *Sender);
private:

		void DoReset();
		void DoContinue();
		void DoFinish();
		void DoAnswer();
		TList *pListBox;
		TList *pListAnswer;
		TList *pRandArr;
		int FCountCorrect;
		int FCountWrong;
		int FNumberCorrect;
//		void mycopy (int* a,int* b, int blen, int num);
		void change_lvl();
		int max_posible;
		int min_posible;
		int aValue();
        int aValue_test();




			// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
	int lvl_index;
};
	const int count_box = 25;
	const int max_answ = 6;
//	const int min_posible = 4;
//	const int max_posible = 14;
	const int K = 2;
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
